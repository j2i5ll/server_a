﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonLib
{
    public class CommandObject
    {
        private string option;

        public string Option
        {
            get { return option; }
            set { option = value; }
        }
        private string cmd;

        public string Cmd
        {
            get { return cmd; }
            set { cmd = value; }
        }
        public CommandObject()
        {
            this.Cmd = "";
            this.Option = "";
        }
    }
}
