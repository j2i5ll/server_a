﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;
using System.Threading;
using CommonLib;
namespace ServerATray.socket
{
    class IPCClientSocket
    {

        private ClientInfo client;
        private int retryCount = 0;
        private const int RETRY_MAX_COUNT = 5;
        private IPEndPoint remoteEP;
     
        System.Windows.Forms.Timer connectTimer;
        worker.Worker worker;
        public IPCClientSocket()
        {
            IPHostEntry ipHostInfo = Dns.Resolve("127.0.0.1");
            this.remoteEP = new IPEndPoint(ipHostInfo.AddressList[0], 30502);
           // IPHostEntry ipHostInfo = Dns.Resolve("165.132.221.43");
            //this.remoteEP = new IPEndPoint(ipHostInfo.AddressList[0], 8001);
            this.client = new ClientInfo();
            this.client.workSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.worker = new worker.Worker();
            
            connectTimer = new System.Windows.Forms.Timer();
            connectTimer.Interval = 2000;
            connectTimer.Tick += new EventHandler(connectTick);
           
        }
        public void connect()
        {
            connectTimer.Start();
        }
        private void connectTick(object sender, EventArgs e)
        {
            this.client.workSocket.BeginConnect(remoteEP, new AsyncCallback(connectCallback), null);
        }
        private void connectCallback(IAsyncResult ar)
        {
            try
            {
                if (retryCount > RETRY_MAX_COUNT)
                {
                    connectTimer.Stop();
                    return;
                }
                
                client.workSocket.EndConnect(ar);
                
                //connected!!
                //MessageBox.Show("connected");
                connectTimer.Stop();
                this.client.workSocket.BeginReceive(this.client.buffer, 0, 1024, 0, new AsyncCallback(receiveCallback), null);
            }
            catch (Exception e)
            {
               MessageBox.Show(e.ToString());
               retryCount++;
            }
        }
        private void receiveCallback(IAsyncResult ar)
        {
            int len = this.client.workSocket.EndReceive(ar);
            //MessageBox.Show("recv");
            if (len > 0)
            {
                this.client.sb.Clear();
                this.client.sb.Append(Encoding.ASCII.GetString(this.client.buffer, 0, len));
                ThreadPool.QueueUserWorkItem(new WaitCallback(worker.toDo), this.client);
                this.client.workSocket.BeginReceive(this.client.buffer, 0, 1024, 0, new AsyncCallback(receiveCallback), null);
            }
            else
            {
                this.client.workSocket.Shutdown(SocketShutdown.Both);
                this.client.workSocket.BeginDisconnect(false, new AsyncCallback(disconnectCallback), null);
            }
        }
        private void disconnectCallback(IAsyncResult ar)
        {
            this.client.workSocket.Close();
        }
    }
}
