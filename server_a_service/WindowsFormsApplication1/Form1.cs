﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ServerATray.socket;

namespace ServerATray
{
    public partial class Form1 : Form
    {
        private bool isRealExit;
        private IPCClientSocket ipcClientSocket;
        public Form1()
        {
            isRealExit = false;
            InitializeComponent();
            this.trayIcon.ContextMenuStrip = this.trayContextMenuStrip;
            ipcClientSocket = new IPCClientSocket();
            ipcClientSocket.connect();
        }

        private void 종료ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isRealExit = true;
            Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isRealExit == false)
            {
                e.Cancel = true;
                this.Visible = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            this.ShowInTaskbar = false;

        }
    }
}
