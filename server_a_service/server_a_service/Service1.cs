﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using server_a_service.sock;
using System.Threading;

namespace server_a_service
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }
        private IPCServerSocket ipcSc;
        private ServerSocket sc;
        protected override void OnStart(string[] args)
        {
            
            sc = new ServerSocket(30501, this);
            ipcSc = new IPCServerSocket(30502, this);
            //sc.listen();
            Thread serverThread = new Thread(new ThreadStart(sc.listen));
            serverThread.IsBackground = true;
            serverThread.Start();

            Thread ipcServerThread = new Thread(new ThreadStart(ipcSc.listen));
            ipcServerThread.IsBackground = true;
            ipcServerThread.Start();
        }
        public void sendToTray(byte[] buff, int len)
        {
            ipcSc.sendToTray(buff, len);
        }

        protected override void OnStop()
        {
        }
    }
}
