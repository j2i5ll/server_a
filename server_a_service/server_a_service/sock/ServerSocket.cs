﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
//using server_a_service.log;
using server_a_service.worker;
using CommonLib;
using System.Windows.Forms;
namespace server_a_service.sock
{
   


    class ServerSocket
    {
        private int port;
        private Socket listener;
        private List<ClientInfo> clients;
        private const int backLog = 100;
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        private Worker worker;
        private Service1 service;
        public ServerSocket(int port, Service1 service)
        {
            this.port = port;
            this.service = service;
            clients = new List<ClientInfo>();
            worker = new Worker();
        }
        public void listen()
        {
            try
            {
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(new IPEndPoint(IPAddress.Any, this.port));
                listener.Listen(backLog);
                while (true)
                {
                    allDone.Reset();
                    Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(new AsyncCallback(acceptCallBack), listener);
                    allDone.WaitOne();
                }


            }
            catch (Exception ex)
            {
                //Log.logger.Error(ex.Message + "\n\r" + ex.StackTrace);
            }

        }

        private void acceptCallBack(IAsyncResult ar)
        {
            try
            {

                allDone.Set();
                ClientInfo state = new ClientInfo();
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);     //client sock
                IPEndPoint ep = (IPEndPoint)handler.RemoteEndPoint;


                state.workSocket = handler;
                clients.Add(state);
                Console.WriteLine("Client Connect from {0}. total : {1} ", ep.Address, clients.Count);
                //Log.logger.Info("Client Connect from " + ep.Address + ". Total Client : " + clients.Count);
                handler.BeginReceive(state.buffer, 0, ClientInfo.BufferSize, 0, new AsyncCallback(readCallBack), state);
            }
            catch (Exception ex)
            {
                //Log.logger.Error(ex.Message + "\n\r" + ex.StackTrace);
            }
        }

        private void readCallBack(IAsyncResult ar)
        {
            string data = string.Empty;
            ClientInfo state = (ClientInfo)ar.AsyncState;
            Socket handler = state.workSocket;

            int bytesRead = 0;
            try
            {
                bytesRead = handler.EndReceive(ar);
            }
            catch (SocketException ex) { }

            if (bytesRead > 0)
            {
                state.sb.Clear();
                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                //Console.WriteLine(state.sb.ToString());
                //Log.logger.Info("Recv Message : [" + state.sb.ToString() + "] from " + ((IPEndPoint)state.workSocket.RemoteEndPoint).Address);
                if (state.sb.ToString().StartsWith("control"))
                {
                   
                    service.sendToTray(state.buffer, bytesRead);
                    
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(worker.toDo), state);
                }
                handler.BeginReceive(state.buffer, 0, ClientInfo.BufferSize, 0, new AsyncCallback(readCallBack), state);
                //handler.BeginSend(

            }
            else            //disconnected
            {
                handler.Shutdown(SocketShutdown.Both);
                handler.BeginDisconnect(false, new AsyncCallback(disconnectCallBack), state);
            }
        }
        private void disconnectCallBack(IAsyncResult ar)
        {
            ClientInfo state = (ClientInfo)ar.AsyncState;

            IPEndPoint ep = (IPEndPoint)state.workSocket.RemoteEndPoint;
            clients.Remove(state);
            //Console.WriteLine("disconnect from {0}. total : {1}", ep.Address, clients.Count);
            //Log.logger.Info("Disconnect from " + ep.Address + ". Total Client : " + clients.Count);
            state.workSocket.Close();

        }
    }
}
