﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using CommonLib;
namespace server_a_service.sock
{
    



    class IPCServerSocket
    {
        private int port;
        private Socket listener;
        private ClientInfo client;
        private const int backLog = 100;
        private Service1 service;
        private byte[] buffer;
        public IPCServerSocket(int port, Service1 service)
        {
            this.port = port;
            this.service = service;
           
            this.client = new ClientInfo();
        }
        public void listen()
        {
            try
            {
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(new IPEndPoint(IPAddress.Loopback, this.port));
                listener.Listen(backLog);
               
                   
                Console.WriteLine("Waiting for a connection...");
                listener.BeginAccept(new AsyncCallback(acceptCallBack), listener);
               
            }
            catch (Exception ex)
            {
                //Log.logger.Error(ex.Message + "\n\r" + ex.StackTrace);
            }
        }

        private void acceptCallBack(IAsyncResult ar)
        {
            try
            {

                
               
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);     //client sock
                IPEndPoint ep = (IPEndPoint)handler.RemoteEndPoint;


                client.workSocket = handler;

                
                
                //Log.logger.Info("Client Connect from " + ep.Address + ". Total Client : " + clients.Count);
                handler.BeginReceive(buffer, 0, 1024, 0, new AsyncCallback(readCallBack), null);
            }
            catch (Exception ex)
            {
                //Log.logger.Error(ex.Message + "\n\r" + ex.StackTrace);
            }
        }

        private void readCallBack(IAsyncResult ar)
        {
            string data = string.Empty;

            Socket handler = client.workSocket;

            int bytesRead = 0;
            try
            {
                bytesRead = handler.EndReceive(ar);
            }
            catch (SocketException ex) { }

            if (bytesRead > 0)
            {
                
                //state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                //Console.WriteLine(state.sb.ToString());
                //Log.logger.Info("Recv Message : [" + state.sb.ToString() + "] from " + ((IPEndPoint)state.workSocket.RemoteEndPoint).Address);


                handler.BeginReceive(buffer, 0, 1024, 0, new AsyncCallback(readCallBack), null);
                //handler.BeginSend(

            }
            else            //disconnected
            {
                handler.Shutdown(SocketShutdown.Both);
                handler.BeginDisconnect(false, new AsyncCallback(disconnectCallBack), null);
            }
        }
        public void sendToTray(byte[] buff, int len)
        {
            if (client != null)
            {
                client.workSocket.BeginSend(buff, 0, len, 0, new AsyncCallback(sendCallback), null);
            }
        }
        private void sendCallback(IAsyncResult ar)
        {
            try
            {
                client.workSocket.EndSend(ar);   
            }
            catch (Exception ex)
            {
            }
        }
        private void disconnectCallBack(IAsyncResult ar)
        {
            client.workSocket.Close();
            listen();

        }
    }
}
