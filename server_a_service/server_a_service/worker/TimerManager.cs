﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using CommonLib;
namespace server_a_service.worker
{
    delegate void TimerHandler(object o, ElapsedEventArgs e);
    class TimerTask : Timer
    {
        private DateTime eventTime;

        public DateTime EventTime
        {
            get { return eventTime; }
            set { eventTime = value; }
        }

        private string cmd;
        public TimerTask(CommandObject co, TimerHandler handler)
            : base()
        {
            //생성은 worker에서 하고 interval 시간또한 worker에서 결정한다.
            //timertask가 생성되고 start될때 까지(worker에서 timertask생성 -> tiemrManager에 add 될때까지의 term) 약간의 term이 있지만
            //크리티컬한 수치가 아니라 생각한다.
            int second = Int32.Parse(co.Option);
            eventTime = DateTime.Now.AddSeconds(second);
            this.Cmd = co.Cmd;
            this.AutoReset = false;
            this.Interval = second * 1000.0;

            this.Elapsed += new ElapsedEventHandler(handler);

        }


        public string Cmd
        {
            get { return cmd; }
            set { cmd = value; }
        }
    }
    class TimerManager
    {
        private List<TimerTask> timerList;
        public TimerManager()
        {
            timerList = new List<TimerTask>();

        }
        public void addTimerTask(TimerTask task)
        {
            //Timer t = new Timer(callback, state, state.Minute, Timeout.Infinite);
            timerList.Add(task);
            timerList.Sort(delegate(TimerTask a, TimerTask b)
            {
                return a.EventTime.CompareTo(b.EventTime);
            });
            task.Start();
        }
        public string getTimerScheduleList()
        {
            string str = "";
            for (int i = 0; i < timerList.Count; i++)
            {
                str += timerList[i].Cmd + "#" + timerList[i].EventTime.ToString("yyyy-MM-dd HH:mm:ss");
                if (i < timerList.Count + 1)
                {
                    str += "@";
                }

            }
            return str;
        }
        public void removeTimerTask(TimerTask task)
        {
            timerList.Remove(task);
        }
    }
}
