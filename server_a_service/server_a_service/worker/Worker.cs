﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Diagnostics;
using server_a_service.sock;
using System.Net.Sockets;
using System.Runtime.InteropServices;
//using server_a_service.log;
using System.Collections;
using System.Windows.Forms;
using CommonLib;
namespace server_a_service.worker
{
   
    class Worker
    {
        private TimerManager tManager;
        private string moviePlayerProc = "PotPlayer";



        public Worker()
        {
            tManager = new TimerManager();


        }

        public void toDo(object ob)
        {
            ClientInfo clientState = (ClientInfo)ob;
            string cmd = clientState.sb.ToString();
            Socket clientSock = clientState.workSocket;
            //Console.WriteLine(cmd);
            CommandObject co = null;
            TimerTask tTask = null;
            co = getCmdObj(cmd);
            if (co.Cmd == "shutdown")
            {
                tTask = new TimerTask(co, shutdown);
                tManager.addTimerTask(tTask);
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                clientSock.Send(encoding.GetBytes("OK\n"));

            }
            else if (co.Cmd == "schlist")
            {
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                clientSock.Send(encoding.GetBytes(tManager.getTimerScheduleList() + "\nOK\n"));
            }
            else if (co.Cmd == "ttt")
            {
                Console.WriteLine("111111");
            }
        }

        private CommandObject getCmdObj(string cmdStr)
        {
            CommandObject co = new CommandObject();
            string[] words = cmdStr.Split(new char[] { '@' });
            if (words.Length == 2)
            {
                co.Cmd = words[0];
                co.Option = words[1];
            }
            else if (words.Length == 1)
            {
                co.Cmd = words[0];
            }


            return co;

        }
        private void shutdown(object task, ElapsedEventArgs e)
        {


            TimerTask tTask = (TimerTask)task;
            //Console.WriteLine("aksk3k3k1k4l1j4");
            releaseTask(tTask);


            ///Log.logger.Info("System Shutdown");
            Process.Start("shutdown", "-s -t 0");
        }
        private void releaseTask(TimerTask task)
        {
            tManager.removeTimerTask(task);
            task.Dispose();
        }
    }
}
